package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class MyOwnDublicateTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();
    @Test
    public void test0() {
        //run
        try {
            Path a = Files.createTempFile("aaa", ".tmp");
            Path b = Files.createTempFile("bbb",".tmp");
            Path example = Files.createTempFile("result", ".tmp");
            List list = Stream.of("3", "2", "2", "3", "1").collect(toList());
            Files.write(a,list);
            list = Stream.of("1[1]", "2[2]", "3[2]").collect(toList());
            Files.write(example,list);
            duplicateFinder.process(a.toFile(), b.toFile());
            List list1 = Files.readAllLines(b);
            List list2 = Files.readAllLines(example);
            boolean result = list1.equals(list2);
            Assert.assertTrue(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
