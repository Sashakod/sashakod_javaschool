package com.tsystems.javaschool.tasks.subsequence;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

    public boolean find(List x, List y) throws IllegalArgumentException{
        if (x == null){
            throw new IllegalArgumentException();
        }
        if (y == null){
            throw new IllegalArgumentException();
        }
            int j = 0;
            int count = 0;
            xfor: for (Object ox : x){
                for (;j<y.size();j++){
                    if (ox.equals(y.get(j))){
                        count++;
                        continue xfor;
                    }
                }
            }
            if (count == x.size()){
                return true;
            }
            return false;
    }

    public static void main(String[] args) {
        Subsequence s = new Subsequence();
        boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b); // Result: true
    }
}
