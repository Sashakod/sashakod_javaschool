package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) throws IllegalArgumentException {
        if (sourceFile == null || targetFile == null){
            throw new IllegalArgumentException();
        }
        try{
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
            String str = null;
            List f1List = new ArrayList();
            while ((str = reader.readLine()) != null){
                f1List.add(str);
            }
            reader.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile,true));
            Set<String> f2Set = new TreeSet<String>(f1List);
            for (String s : f2Set){
                writer.write(s+"["+Collections.frequency(f1List,s)+"]\n");
            }
            writer.flush();
            writer.close();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    public static void main(String[] args){
        DuplicateFinder d = new DuplicateFinder();
        d.process(new File("a.txt"),new File("b.txt"));
    }




}
