package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        LinkedList<Double> st = new LinkedList<Double>();
        LinkedList<Character> op = new LinkedList<Character>();
        try{
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (isSpace(c))
                    continue;
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        processOperator(st, op.removeLast());
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        processOperator(st, op.removeLast());
                    op.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || (i != 0 && Character.isDigit(statement.charAt(i-1)) && statement.charAt(i) == '.')))
                        operand += statement.charAt(i++);
                    --i;
                    st.add(Double.parseDouble(operand));
                }
            }
            while (!op.isEmpty())
                processOperator(st, op.removeLast());
            return new BigDecimal(st.get(0)).setScale(4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
        }catch (Exception e){
            return null;
        }
    }

    private static boolean isSpace(char c) {
        return c == ' ';
    }

    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    private static void processOperator(LinkedList<Double> st, char op) {
        Double r = st.removeLast();
        Double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                st.add(l / r);
        }
    }

    public static void main(String[] args) {
        Calculator c = new Calculator();
//        System.out.println(c.evaluate("(1.0253+38.04873)*4.014563-5.9874503")); // Result: 151
//        System.out.println(c.evaluate("7*6/2+8")); // Result: 29
//        System.out.println(c.evaluate("-12)1//(")); // Result: null
//        System.out.println(c.evaluate("27.11906+75.0045"));
//        System.out.println(c.evaluate("(1 + 38) * 4.5 - 1 / 2."));
        System.out.println(c.evaluate("10/2-7+3*4"));

    }

}
